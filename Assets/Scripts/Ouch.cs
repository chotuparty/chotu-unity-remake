﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ouch : MonoBehaviour{



    // Start is called before the first frame update
    void Start(){
        
    }

    // Update is called once per frame
    void Update(){
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began)) {
            Debug.Log("Touching");

            Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
            if (hit.collider != null) {
                Debug.Log(hit.collider.name);
                GetComponent<AudioSource>().Play();
            }
        }
    }
}
