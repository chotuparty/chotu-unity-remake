﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyebrowController : MonoBehaviour {
    private Animator animator;
    private readonly int NO_EYEBROWS = 0;
    private readonly int ANGRY = 3, SURPRISED = 2;

    // Start is called before the first frame update
    void Start() {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
    }

    //Function will be called from Java
    //Controls mouth state
    public void SetEmotion(string emotion) {
        switch (emotion) {
            case "ANGRY":
                animator.SetInteger("emotion", ANGRY);
                break;
            case "SAD":
            case "HAPPY":
                animator.SetInteger("emotion", NO_EYEBROWS);
                break;
            case "SURPRISED":
                animator.SetInteger("emotion", SURPRISED);
                break;
            case "SPEAKING":
                //speaking doesn't change the eyebrows state 
                //This allows for surprised speaking and angry speaking
                break;
        }
    }
}

